﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace AwsIntegration
{
	public class Settings
	{
		private static Settings settings;

		public static Settings Current
		{
			get
			{
				if (settings == null)
				{
					settings = new Settings();
				}
				return settings;
			}
		}

		public string AWSAccessKey
		{
			get
			{
				return ConfigurationManager.AppSettings["AWSAccessKey"];
			}
		}

		public string AWSSecretKey
		{
			get
			{
				return ConfigurationManager.AppSettings["AWSSecretKey"];
			}
		}

		public string S3BucketName
		{
			get
			{
				return ConfigurationManager.AppSettings["AWSS3Bucket"];
			}
		}

		public string S3BucketUrl
		{
			get
			{
				return "//" + S3BucketName + ".s3.amazonaws.com";
			}
		}

		public string CloudFrontDomain
		{
			get
			{
				return ConfigurationManager.AppSettings["AWSCloudFrontDomainName"];
			}
		}

		public string FileProperty
		{
			get
			{
				return "umbracoFile";
			}
		}

		public string S3LinkProperty
		{
			get
			{
				return "awsS3Link";
			}
		}

		public bool IsDebug
		{
			get
			{
				CompilationSection compilationSection = (CompilationSection)System.Configuration.ConfigurationManager.GetSection(@"system.web/compilation");
				//return compilationSection.Debug;
				return false;
			}
		}
	}
}
