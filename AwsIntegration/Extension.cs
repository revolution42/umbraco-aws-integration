﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Umbraco.Core;
using Umbraco.Core.Models;



namespace AwsIntegration
{
	public static class Extension
	{
		public static string GetCDNCropUrl(this umbraco.cms.businesslogic.media.Media media, string propertyName, string cropName)
		{
			var newMedia = ApplicationContext.Current.Services.MediaService.GetById(media.Id);
			return newMedia.GetCDNCropUrl(propertyName, cropName);
		}

		public static string GetCDNCropUrl(this IMedia media, string propertyName, string cropName)
		{
			string cropUrl = string.Empty;

			if (!string.IsNullOrEmpty(media.GetValue<string>(propertyName)))
			{
				var cropXml = media.GetValue<string>(propertyName);
				var xmlDocument = new XmlDocument();
				xmlDocument.LoadXml(cropXml);
				var cropNode = xmlDocument.SelectSingleNode(string.Concat("descendant::crops/crop[@name='", cropName, "']"));

				if (cropNode != null)
				{
					cropUrl = cropNode.Attributes.GetNamedItem("url").InnerText;
				}

				cropUrl = GetUrlFromString(media, cropUrl);
			}

			return cropUrl;
		}




		public static string GetCDNUrl(this umbraco.cms.businesslogic.media.Media media)
		{
			var newMedia = ApplicationContext.Current.Services.MediaService.GetById(media.Id);

			return newMedia.GetCDNUrl();
		}


		public static string GetCDNUrl(this IMedia media)
		{
			var url = media.GetValue<string>(Settings.Current.FileProperty);
			return GetUrlFromString(media, url);
		}

		private static string GetUrlFromString(IMedia media, string url)
		{
			var cloudFrontDomain = Settings.Current.CloudFrontDomain;
			if (!Settings.Current.IsDebug)
			{
				if (!string.IsNullOrEmpty(cloudFrontDomain))
				{
					url = "//" + cloudFrontDomain + url;
				}
				else if (media.HasProperty(Settings.Current.S3LinkProperty) && !string.IsNullOrEmpty(media.GetValue<string>(Settings.Current.S3LinkProperty)))
				{
					var prop = media.GetValue<string>(Settings.Current.S3LinkProperty);
					url = media.GetValue<string>(Settings.Current.S3LinkProperty) + GetFilename(url);
				}
			}

			return url;
		}


		private static string GetFilename(string path)
		{
			var array = path.Split('/');
			return array.Last();
		}
	}
}
