﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AwsIntegration.Usercontrols.ManageAWSIntegration.ManageAWSIntegration.ascx.cs" Inherits="AwsIntegration.Usercontrols.ManageAWSIntegration" %>
<div class="propertypane" style="">
	<div>
		<div class="propertyItem" style="">

			<div class="dashboardWrapper">
				<h2>Manage AWS Integration</h2>
				<h3>Basic S3 Upload</h3>
				<p>This will uploaded files that have not been uploaded already.</p>
				<asp:Button ID="BasicUpload" runat="server" Text="Basic Upload" OnClick="BasicUpload_Click"/>

				<h3>Re Upload All Files</h3>
				<p>This will reuploaded everyfile to S3. If you have a lot of files it will take some time.</p>
				<asp:Button ID="ReUpload" runat="server" Text="Upload Everything" OnClick="ReUpload_Click" />
			</div>
		</div>
		<div class="propertyPaneFooter">-</div>
	</div>
</div>
