﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using umbraco;
using umbraco.cms.businesslogic.media;
using Umbraco.Core.Models;
using Umbraco.Web;


namespace AwsIntegration.Usercontrols
{
	public partial class ManageAWSIntegration : UmbracoUserControl
	{
		protected void BasicUpload_Click(object sender, EventArgs e)
		{
			foreach (var media in ListMedia())
			{
				if (media.HasProperty(Settings.Current.S3LinkProperty) && String.IsNullOrEmpty(Settings.Current.S3LinkProperty))
				{
					new S3MediaUpload(media);
				}
			}
		}

		protected void ReUpload_Click(object sender, EventArgs e)
		{
			foreach (var media in ListMedia())
			{
				if (media.HasProperty(Settings.Current.S3LinkProperty) )
				{
					new S3MediaUpload(media);
				}
			}
		}

		private IEnumerable<umbraco.cms.businesslogic.media.Media> ListMedia()
		{
			return uQuery.GetMediaByXPath("//*");
		}

	}
}