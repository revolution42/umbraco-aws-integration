﻿using System.Web;
using Umbraco.Core;

namespace AwsIntegration
{
	public class AwsMedia : IApplicationEventHandler
	{

		void Media_AfterSave(umbraco.cms.businesslogic.media.Media media, umbraco.cms.businesslogic.SaveEventArgs e)
		{
			if (!Settings.Current.IsDebug)
			{
				// Only upload if uploading from web, we don't want to upload from the thread
				if (HttpContext.Current != null)
				{
					new S3MediaUpload(media);
				}
			}
		}
		
		void IApplicationEventHandler.OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
		{
			umbraco.cms.businesslogic.media.Media.AfterSave += Media_AfterSave;
		}

		void IApplicationEventHandler.OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
		{
			//throw new System.NotImplementedException();
		}


		void IApplicationEventHandler.OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
		{
			//throw new System.NotImplementedException();
		}
	}
}
