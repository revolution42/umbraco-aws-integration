﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace AwsIntegration
{
	public class S3MediaUpload : UmbracoUserControl
	{
		[Obsolete("Still uses umbraco.cms.businesslogic.media.Media")]
		public S3MediaUpload(umbraco.cms.businesslogic.media.Media uploadMedia)
		{
			var mediaService = Services.MediaService;
			DoUpload(mediaService.GetById(uploadMedia.Id));
		}
		
		public S3MediaUpload(IMedia uploadMedia)
		{
			DoUpload(uploadMedia);
		}

		private void DoUpload(IMedia uploadMedia)
		{
			if (uploadMedia.HasProperty(Settings.Current.S3LinkProperty))
			{
				var serverBasePath = HttpContext.Current.Server.MapPath("~");
				var filePath = uploadMedia.GetValue<string>(Settings.Current.FileProperty);
				var actualFilePath = HttpContext.Current.Server.MapPath(filePath);
				var directory = Path.GetDirectoryName(actualFilePath);
				var uploadUrl = Settings.Current.S3BucketUrl + filePath;
				var transferUtility = new Amazon.S3.Transfer.TransferUtility(Settings.Current.AWSAccessKey, Settings.Current.AWSSecretKey);

				foreach (var file in Directory.GetFiles(directory))
				{
					var uploadRequest = new TransferUtilityUploadRequest();
					uploadRequest.FilePath = file;
					uploadRequest.BucketName = Settings.Current.S3BucketName;
					uploadRequest.Key =  NormalizeKey(file.Replace(serverBasePath, ""));
					uploadRequest.CannedACL = S3CannedACL.PublicRead;
					uploadRequest.AutoCloseStream = true;
					uploadRequest.Timeout = 36000000;
					transferUtility.BeginUpload(uploadRequest, null, null);
				}

				uploadMedia.SetValue(Settings.Current.S3LinkProperty, "/" + NormalizeKey(Settings.Current.S3BucketUrl + "/" + directory.Replace(serverBasePath, "").Replace("\\", "/") + "/"));
				var mediaService = Services.MediaService;
				mediaService.Save(uploadMedia);
			}
		}

		private static string NormalizeKey(string relativePath)
		{
			return relativePath.Replace("~/", "").Replace(@"~\", "").Replace(@"\", @"/").Replace(@"//", @"/");
		}

		private void CreateFolder(AmazonS3 client, string folder)
		{
			string bucket = Settings.Current.S3BucketName;
			var key = string.Format(@"{0}/", folder);
			var request = new PutObjectRequest().WithBucketName(bucket).WithKey(key);
			request.InputStream = new MemoryStream();
			client.PutObject(request);
		}
	}

}
